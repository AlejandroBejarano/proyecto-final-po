import requests,bs4
import matplotlib.pyplot as plt

def get_vacancies(years_of_experience):
    url = "https://www.linkedin.com/jobs/search/?geoId=1022488&keywords=ingeniero%20catastral%20y%20geodesista"
    response = requests.get(url)
    soup = bs4.BeautifulSoup(response.content, "html.parser")
    vacancies = soup.find_all("a", class_="job-result__title")
    filtered_vacancies = []
    for vacancy in vacancies:
        experience_string = vacancy.find("span", class_="job-result__job-years").text
        if experience_string and int(experience_string) >= years_of_experience:
            filtered_vacancies.append(vacancy)
    return filtered_vacancies

def calculate_shortfall(vacancies, professionals):
    shortfall = len(vacancies) - len(professionals)
    return shortfall

def plot_shortfall_by_month(shortfalls):
    months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    plt.plot(months, shortfalls)
    plt.xlabel("Mes")
    plt.ylabel("Déficit")
    plt.title("Déficit de ingenieros catastrales y geodesistas por mes")
    plt.show()

if __name__ == "__main__":
    # Obtener el número de años de experiencia
    years_of_experience = int(input("Ingrese el número de años de experiencia: "))

    # Obtener el número de vacantes
    vacancies = get_vacancies(years_of_experience)

    # Obtener el número de profesionales
    professionals = 1000

    # Calcular el déficit
    shortfall = calculate_shortfall(vacancies, professionals)

    # Graficar el déficit por mes
    shortfall_by_month = []
    for i in range(12):
        shortfall_by_month.append(shortfall)
    plot_shortfall_by_month(shortfall_by_month)

    # Mostrar los resultados
    print("Hay {} vacantes para ingenieros catastrales y geodesistas en Bogotá DC con {} años de experiencia o más.".format(len(vacancies), years_of_experience))
    print("Hay {} ingenieros catastrales y geodesistas en Bogotá DC.".format(professionals))
    print("El déficit es {}.".format(shortfall))
